
<?php
require_once 'init.php';
if(isset($_POST['add_category']))
{
    if(Util::verifyCSRFToken($_POST))
    {
        
        $result = $di->get('category')->addCategory($_POST);
        
        switch($result)
        {
            case ADD_ERROR:
                Session::setSession(ADD_ERROR,"Add Category Error");
                Util::redirect("manage-category.php");
                break;
            case ADD_SUCCESS:
                Session::setSession(ADD_SUCCESS,"Add Category Success");
                Util::redirect("manage-category.php");
                break;
            case VALIDATION_ERROR:
                Session::setSession('validation',"Validation Error");
                Session::setSession('old',$_POST);
                ?>
                <?php
                Session::setSession('errors',serialize($di->get('category')->getValidator()->errors()));//object mai hai ya array hai to text mai store kar sakeee!
                Util::redirect("add-category.php");
                break;
        }
    }else{
        //errorpage 
        Session::setSession("csrf","CSRF ERROR");
        Util::redirect("manage-category.php");//Need to change this, actually we be redirecting to some error page indicating Unauthorized access.

    }
}
if(isset($_POST['add_customer']))
{   
    if(Util::verifyCSRFToken($_POST))
    {
        
        $result = $di->get('customer')->addCustomer($_POST);
        
        switch($result)
        {
            case ADD_ERROR:
                Session::setSession(ADD_ERROR,"Add Customer Error");
                Util::redirect("manage-customer.php");
                break;
            case ADD_SUCCESS:
                Session::setSession(ADD_SUCCESS,"Add Customer Success");
                Util::redirect("manage-customer.php");
                break;
            case VALIDATION_ERROR:
                Session::setSession('validation',"Validation Error");
                Session::setSession('old',$_POST);
           
                Session::setSession('errors',serialize($di->get('customer')->getValidator()->errors()));
                Util::redirect("add-customer.php");
                break;
        }
    }else{
        //errorpage 
        Session::setSession("csrf","CSRF ERROR");
        Util::redirect("manage-customer.php");//Needto change this, actually we be redirecting to some error page indicating Unauthorized access.

    }
}

if(isset($_POST['add_supplier']))
{   
    if(Util::verifyCSRFToken($_POST))
    {
        $result = $di->get('supplier')->addSupplier($_POST);
        
        switch($result)
        {
            case ADD_ERROR:
                Session::setSession(ADD_ERROR,"Add Supplier Error");
                Util::redirect("manage-supplier.php");
                break;
            case ADD_SUCCESS:
                Session::setSession(ADD_SUCCESS,"Add Supplier Success");
                Util::redirect("manage-supplier.php");
                break;
            case VALIDATION_ERROR:
                Session::setSession('validation',"Validation Error");
                Session::setSession('old',$_POST);
           
                Session::setSession('errors',serialize($di->get('supplier')->getValidator()->errors()));
                Util::redirect("add-supplier.php");
                break;
        }
    }else{
        //errorpage 
        Session::setSession("csrf","CSRF ERROR");
        Util::redirect("manage-supplier.php");//Needto change this, actually we be redirecting to some error page indicating Unauthorized access.

    }
}

if(isset($_POST['add_product']))
{   
    if(Util::verifyCSRFToken($_POST))
    {
        $result = $di->get('product')->addProduct($_POST);
        switch($result)
        {
            case ADD_ERROR:
                Session::setSession(ADD_ERROR,"Add Product Error");
                Util::redirect("manage-product.php");
                break;
            case ADD_SUCCESS:
                Session::setSession(ADD_SUCCESS,"Add Product Success");
                Util::redirect("manage-product.php");
                break;
            case VALIDATION_ERROR:
                Session::setSession('validation',"Validation Error");
                Session::setSession('old',$_POST);
           
                Session::setSession('errors',serialize($di->get('product')->getValidator()->errors()));
                Util::redirect("add-product.php");
                break;
        }
    }else{
        //errorpage 
        Session::setSession("csrf","CSRF ERROR");
        Util::redirect("manage-product.php");//Needto change this, actually we be redirecting to some error page indicating Unauthorized access.

    }
}



if(isset($_POST['page']))
{
    if($_POST['page'] == 'manage_category')
    {
        $dependency = 'category';
    }
    elseif($_POST['page'] == 'manage_customer'){
        
        $dependency = 'customer';
    }
    elseif($_POST['page'] == 'manage_supplier'){
        
        $dependency = 'supplier';
    }
    elseif($_POST['page'] == 'manage_product'){
        
        $dependency = 'product';
    }
    // Util::dd($dependency);
    $search_parameter = $_POST['search']['value'] ?? null;
    $order_by = $_POST['order'] ?? null;
    $start = $_POST['start'];
    $length = $_POST['length'];
    $draw = $_POST['draw'];
    
    $di->get($dependency)->getJSONDataForDataTable($draw,$search_parameter,$order_by,$start,$length);
    
}

if(isset($_POST['fetch']))
{
    if($_POST['fetch'] == 'category')
    {
        $category_id = $_POST['category_id'];
        $result = $di->get('category')->getCategoryByID($category_id,PDO::FETCH_ASSOC);
        echo json_encode($result[0]);
    }
    if($_POST['fetch'] == 'customer')
    {
        $customer_id = $_POST['customer_id'];
        $result = $di->get('customer')->getCustomerByID($customer_id,PDO::FETCH_ASSOC);
        // Util::dd($result);
        echo json_encode($result[0]);
    }
}
if(isset($_POST['editCategory']))
{
    if(Util::verifyCSRFToken($_POST))
    {
        
        $result = $di->get('category')->update($_POST,$_POST['category_id']);
        
        switch($result)
        {
            case UPDATE_ERROR:
                Session::setSession(UPDATE_ERROR,"Update Category Error");
                Util::redirect("manage-category.php");
                break;
            case UPDATE_SUCCESS:
                Session::setSession(UPDATE_SUCCESS,"Update Category Success");
                Util::redirect("manage-category.php");
                break;
            case VALIDATION_ERROR:
                Session::setSession('validation',"Validation Error");
                Session::setSession('old',$_POST);
                Session::setSession('errors',serialize($di->get('category')->getValidator()->errors()));//object mai hai ya array hai to text mai store kar sakeee!
                Util::redirect("manage-category.php");
                break;
        }
    }else{
        //errorpage 
        Session::setSession("csrf","CSRF ERROR");
        Util::redirect("manage-category.php");//Need to change this, actually we be redirecting to some error page indicating Unauthorized access.

    }
}

if(isset($_GET['editCustomer']))
{   
    if(Util::verifyCSRFToken($_GET))
    {
        
        // $customer_id = $_POST['id'];
        $result = $di->get('customer')->update($_GET, $_GET['id']);
        // Util::dd($result);
        switch($result)
        {
            case UPDATE_ERROR:
                Session::setSession(UPDATE_ERROR,"Edit Customer Error");
                Util::redirect("manage-customer.php");
                break;
            case UPDATE_SUCCESS:
                Session::setSession(UPDATE_SUCCESS,"Edit Customer Success");
                Util::redirect("manage-customer.php");
                break;
            case VALIDATION_ERROR:
                Session::setSession('validation',"Validation Error");
                Session::setSession('old',$_GET);
           
                Session::setSession('errors',serialize($di->get('customer')->getValidator()->errors()));
                
                // header("Location: {BASEPAGES}edit-customer.php");
                Util::redirect("edit-customer.php?id=".$_GET['id']);
                break;
        }
    }else{
        //errorpage 
        Session::setSession("csrf","CSRF ERROR");
        Util::redirect("manage-customer.php");//Needto change this, actually we be redirecting to some error page indicating Unauthorized access.

    }
}
if(isset($_POST['deleteCategory']))
{
if(Util::verifyCSRFToken($_POST))
{   
    // Util::dd($_POST['record_id']);
    $result = $di->get('category')->delete($_POST['record_id']);

    
    switch($result)
    {
        case DELETE_ERROR:
            Session::setSession(DELETE_ERROR,"Delete Category Error");
            Util::redirect("manage-category.php");
            break;
            case DELETE_SUCCESS:
                Session::setSession(DELETE_SUCCESS,"Delete Category Success");
                Util::redirect("manage-category.php");
                break;
    }
}
else{
    //errorpage 
    Session::setSession("csrf","CSRF ERROR");
    Util::redirect("manage-category.php");//Need to change this, actually we be redirecting to some error page indicating Unauthorized access.

}
}

if(isset($_POST['deleteCustomer']))
{
if(Util::verifyCSRFToken($_POST))
{   
    // Util::dd($_POST['record_id']);
    $result = $di->get('customer')->delete($_POST['record_id']);

    
    switch($result)
    {
        case DELETE_ERROR:
            Session::setSession(DELETE_ERROR,"Delete Customer Error");
            Util::redirect("manage-customer.php");
            break;
            case DELETE_SUCCESS:
                Session::setSession(DELETE_SUCCESS,"Delete Customer Success");
                Util::redirect("manage-customer.php");
                break;
    }
}
else{
    //errorpage 
    Session::setSession("csrf","CSRF ERROR");
    Util::redirect("manage-customer.php");//Need to change this, actually we be redirecting to some error page indicating Unauthorized access.

}
}


//Annonympus Routing Ajax  Routing to Fetch Data
if(isset($_POST['getCategories']))
{
    echo json_encode($di->get('category')->all());
}

if(isset($_POST['getProductsByCategoryID']))
{
    $category_id = $_POST['categoryID'];
    // Util::dd('hello');
    echo json_encode($di->get('product')->getProductsByCategoryID($category_id));    
}

if(isset($_POST['getPriceByProductID']))
{
    // Util::dd($_POST);
    $product_id = $_POST['productID'];
    echo json_encode($di->get('product')->getPriceByProductID($product_id));
}

if(isset($_POST['getCustomerWithEmail']))
{
    // Util::dd("heelloo");
    $email_id = $_POST['emailID'];
    echo json_encode($di->get('customer')->getCustomerWithEmail($email_id));
}

if(isset($_POST['add_sales']))
{
    
    if(Util::verifyCSRFToken($_POST))
    {   
        $result = $di->get('sales')->create($_POST);
        $invoice_id =$di->get('sales')->getInvoiceID();
        switch($result)
        {
            case ADD_ERROR:
                Session::setSession(ADD_ERROR,"Add Product Error");
                Util::redirect("add-sales.php");
                break;
            case ADD_SUCCESS:
                Session::setSession(ADD_SUCCESS,"Add Product Success");
                Util::redirect("invoice.php?id=$invoice_id");
                break;
            case VALIDATION_ERROR:
                Session::setSession('validation',"Validation Error");
                Session::setSession('old',$_POST);
           
                Session::setSession('errors',serialize($di->get('product')->getValidator()->errors()));
                Util::redirect("add-sales.php");
                break;
        }
    }
    else{
        //errorpage 
        Session::setSession("csrf","CSRF ERROR");
        Util::redirect("manage-customer.php");//Need to change this, actually we be redirecting to some error page indicating Unauthorized access.

    }
}


?>