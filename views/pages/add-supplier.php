<?php
    require_once __DIR__."/../../helper/init.php";
    $page_title ="Quick ERP | ADD SUPPLIER";
    $sidebarSection = 'supplier';
    $sidebarSubSection = 'add';
    Util::createCSRFToken();
    $errors="";
    $old="";
    if(Session::hasSession('old'))
    {
      $old = Session::getSession('old');
      Session::unsetSession('old');
    }
    if(Session::hasSession('errors'))
    {
      $errors = unserialize(Session::getSession('errors'));
      Session::unsetSession('errors');
    }
?>
<!DOCTYPE html>
<html lang="en">

<head>

  <?php
    require_once __DIR__."/../includes/head-section.php";
  ?>
</head>

<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">

    <!-- Sidebar -->
    <?php require_once __DIR__."/../includes/sidebar.php"; ?>
    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

        <!-- Topbar -->
          <?php require_once __DIR__."/../includes/navbar.php"; ?>
        <!-- End of Topbar -->

        <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
            <div class="d-sm-flex align-items-center justify-content-between mb-4">
                <h1 class="h3 mb-0 text-gray-800">Add Supplier</h1>
                <a href="#" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm">
                    <i class="fas fa-list-ul fa-sm text-white"></i>Manage Supplier</a>
            </div>
        </div>
        <!-- /.container-fluid -->

        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card show mb-4">
                        <div class="card-header">
                            <h6 class="m-0 font-weight-bold text-primary">
                                    <i class="fa fa-plus"></i>Add Supplier
                            </h6>
                        </div>
                        <!--END OF CARD HEADER-->

                        <!--CARD BODY-->
                        <div class="card-body">
                          <form id="add-supplier" action="<?= BASEURL?>helper/routing.php" method="POST">
                            <input type="hidden"
                              name="csrf_token"
                              value="<?= Session::getSession('csrf_token');?>">
                        <!--Name   -->
                           <!--first name -->
                           <div class="row">
                              <div class="col-md-6">
                                <div class="form-group">
                                  <label for="first_name">First Name</label>
                                  <input type="text" 
                                    class="form-control <?= $errors!= '' ? ($errors->has('first_name') ? 'error is-invalid' : '') : '';?>"
                                    name="first_name"
                                    id="first_name"  
                                    placeholder="Enter First Name"
                                    value="<?= $old != '' ?$old['first_name']: '';?>"
                                  >
                                 </div>
                                <?php
                                if($errors!="" && $errors->has('first_name')):
                                  echo "<span class='error'> {$errors->first('first_name')}</span>";
                                endif;
                                ?> 
                              </div>
                              <!--first name -->

                              <!--last name -->
                           
                              <div class="col-md-6">
                                <div class="form-group">
                                  <label for="last_name">Last Name</label>
                                  <input type="text" 
                                    class="form-control <?= $errors!= '' ? ($errors->has('last_name') ? 'error is-invalid' : '') : '';?>"
                                    name="last_name"
                                    id="last_name"  
                                    placeholder="Enter Last Name"
                                    value="<?= $old != '' ?$old['last_name']: '';?>"
                                  >
                                 </div>
                                <?php
                                if($errors!="" && $errors->has('last_name')):
                                  echo "<span class='error'> {$errors->first('last_name')}</span>";
                                endif;
                                ?> 
                              </div>
                            </div>
                            <!--first name -->

                          <!-- End of name -->

                          <!-- GST and Phone Number -->
                              <!-- gst number -->
                             <div class="row">
                              <div class="col-md-6">
                                <div class="form-group">
                                  <label for="gst_no">GST Number</label>
                                  <input type="text" 
                                    class="form-control <?= $errors!= '' ? ($errors->has('gst_no') ? 'error is-invalid' : '') : '';?>"
                                    name="gst_no"
                                    id="gst_no"  
                                    placeholder="Enter GST Number"
                                    value="<?= $old != '' ?$old['gst_no']: '';?>"
                                  >
                                </div>
                                <?php
                                if($errors!="" && $errors->has('gst_no')):
                                  echo "<span class='error'> {$errors->first('gst_no')}</span>";
                                endif;
                                ?> 
                              </div>
                            <!-- /gst number -->

                             <!-- phone number -->
                              <div class="col-md-6">
                                <div class="form-group">
                                  <label for="phone_no">Phone Number</label>
                                  <input type="number" 
                                    class="form-control <?= $errors!= '' ? ($errors->has('phone_no') ? 'error is-invalid' : '') : '';?>"
                                    name="phone_no"
                                    id="phone_no"  
                                    placeholder="Enter Phone Number"
                                    value="<?= $old != '' ?$old['phone_no']: '';?>"
                                  >
                                </div>
                                <?php
                                if($errors!="" && $errors->has('phone_no')):
                                  echo "<span class='error'> {$errors->first('phone_no')}</span>";
                                endif;
                                ?> 
                              </div>
                            </div>
                            <!-- /phone number -->

                            <!-- /End of GST and phone Number -->
                          
                          <!-- Email and gender -->
                            <!-- email -->
                            <div class="row">
                              <div class="col-md-6">
                                <div class="form-group">
                                  <label for="email_id">Email</label>
                                  <input type="email" 
                                    class="form-control <?= $errors!= '' ? ($errors->has('email_id') ? 'error is-invalid' : '') : '';?>"
                                    name="email_id"
                                    id="email_id"  
                                    placeholder="Enter Your Email"
                                    value="<?= $old != '' ?$old['email_id']: '';?>"
                                  >
                                </div>
                                <?php
                                if($errors!="" && $errors->has('email_id')):
                                  echo "<span class='error'> {$errors->first('email_id')}</span>";
                                endif;
                                ?> 
                              </div>
                            <!-- /email -->

                            <!--company name -->
                           
                            <div class="col-md-6">
                                <div class="form-group">
                                  <label for="company_name">Company Name</label>
                                  <input type="text" 
                                    class="form-control <?= $errors!= '' ? ($errors->has('company_name') ? 'error is-invalid' : '') : '';?>"
                                    name="company_name"
                                    id="company_name"  
                                    placeholder="Enter Companyt Name"
                                    value="<?= $old != '' ?$old['company_name']: '';?>"
                                  >
                                 </div>
                                <?php
                                if($errors!="" && $errors->has('company_name')):
                                  echo "<span class='error'> {$errors->first('company_name')}</span>";
                                endif;
                                ?> 
                              </div>
                            </div>
                            <!--/company name -->
                              
                            <!-- ADDRESS -->

                              <!--block no name -->
                           <div class="row">
                              <div class="col-md-12">
                                <div class="form-group">
                                  <label for="block_no">Block Number</label>
                                  <input type="text" 
                                    class="form-control <?= $errors!= '' ? ($errors->has('block_no') ? 'error is-invalid' : '') : '';?>"
                                    name="block_no"
                                    id="block_no"  
                                    placeholder="Enter Block Number and Building Name"
                                    value="<?= $old != '' ?$old['block_no']: '';?>"
                                  >
                                 </div>
                                <?php
                                if($errors!="" && $errors->has('block_no')):
                                  echo "<span class='error'> {$errors->first('block_no')}</span>";
                                endif;
                                ?> 
                              </div>
                           </div>
                              <!--blok no name -->
                          <!-- street city -->
                           <!--street name -->
                           <div class="row">
                              <div class="col-md-6">
                                <div class="form-group">
                                  <label for="street">Street</label>
                                  <input type="text" 
                                    class="form-control <?= $errors!= '' ? ($errors->has('street') ? 'error is-invalid' : '') : '';?>"
                                    name="street"
                                    id="street"  
                                    placeholder="Enter Street"
                                    value="<?= $old != '' ?$old['street']: '';?>"
                                  >
                                 </div>
                                <?php
                                if($errors!="" && $errors->has('street')):
                                  echo "<span class='error'> {$errors->first('street')}</span>";
                                endif;
                                ?> 
                              </div>
                              <!--street name -->

                              <!--city name -->
                           
                              <div class="col-md-6">
                                <div class="form-group">
                                  <label for="city">City</label>
                                  <input type="text" 
                                    class="form-control <?= $errors!= '' ? ($errors->has('city') ? 'error is-invalid' : '') : '';?>"
                                    name="city"
                                    id="city"  
                                    placeholder="Enter City"
                                    value="<?= $old != '' ?$old['city']: '';?>"
                                  >
                                 </div>
                                <?php
                                if($errors!="" && $errors->has('city')):
                                  echo "<span class='error'> {$errors->first('city')}</span>";
                                endif;
                                ?> 
                              </div>
                            </div>
                            <!--city name -->

                          <!-- End of street city -->

                          <!-- Pincode state country -->
                                <!--pincode -->
                           <div class="row">
                              <div class="col-md-4">
                                <div class="form-group">
                                  <label for="pincode">Pincode</label>
                                  <input type="text" 
                                    class="form-control <?= $errors!= '' ? ($errors->has('pincode') ? 'error is-invalid' : '') : '';?>"
                                    name="pincode"
                                    id="pincode"  
                                    placeholder="Enter pincode"
                                    value="<?= $old != '' ?$old['pincode']: '';?>"
                                  >
                                 </div>
                                <?php
                                if($errors!="" && $errors->has('pincode')):
                                  echo "<span class='error'> {$errors->first('pincode')}</span>";
                                endif;
                                ?> 
                              </div>
                              <!--pincode -->
                              <!--state name -->
                              <div class="col-md-4">
                                <div class="form-group">
                                  <label for="state">State</label>
                                  <input type="text" 
                                    class="form-control <?= $errors!= '' ? ($errors->has('state') ? 'error is-invalid' : '') : '';?>"
                                    name="state"
                                    id="state"  
                                    placeholder="Enter State"
                                    value="<?= $old != '' ?$old['state']: '';?>"
                                  >
                                 </div>
                                <?php
                                if($errors!="" && $errors->has('state')):
                                  echo "<span class='error'> {$errors->first('state')}</span>";
                                endif;
                                ?> 
                              </div>
                              <!--state name -->
                              <!--country name -->
                           
                              <div class="col-md-4">
                                <div class="form-group">
                                  <label for="country">Country</label>
                                  <input type="text" 
                                    class="form-control <?= $errors!= '' ? ($errors->has('country') ? 'error is-invalid' : '') : '';?>"
                                    name="country"
                                    id="country"  
                                    placeholder="Enter Country"
                                    value="<?= $old != '' ?$old['country']: '';?>"
                                  >
                                 </div>
                                <?php
                                if($errors!="" && $errors->has('country')):
                                  echo "<span class='error'> {$errors->first('country')}</span>";
                                endif;
                                ?> 
                              </div>
                            </div>
                            <!--city name -->

                          <!-- End of street city -->

                          <!-- /pincode state country -->

                              <!-- /Address -->

                            <input type="submit" class="btn btn-primary" name="add_supplier" value="submit">
                          </form>
                        </div>
                        <!--END OF CARD BODY-->
                    </div>
                </div>
            </div>
        </div>
      <!-- End of Main Content -->

      </div>
      <!-- End of Main Content -->


      <!-- Footer -->
      <?php require_once __DIR__."/../includes/footer.php"; ?>
      <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  <?php require_once __DIR__."/../includes/scroll-to-top.php"; 
  ?>

 
  <?php require_once __DIR__."/../includes/core-scripts.php"; ?>

  <?php require_once __DIR__."/../includes/page-level/index-scripts.php"; ?>

</body>

</html>
