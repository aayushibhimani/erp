<?php
require_once __DIR__."/../../helper/init.php";
$page_title ="Quick ERP | Add New Sales";
    $sidebarSection = 'transaction';
    $sidebarSubSection = 'sales';
    Util::createCSRFToken();
  $errors="";
  $old="";
  if(Session::hasSession('old'))
  {
    $old = Session::getSession('old');
    Session::unsetSession('old');
  }
  if(Session::hasSession('errors'))
  {
    $errors = unserialize(Session::getSession('errors'));
    Session::unsetSession('errors');
  }
?>

<!DOCTYPE html>
<html lang="en">

<head>

  <?php
    require_once __DIR__."/../includes/head-section.php";
  ?>
  <style>
      .email-verify{
          background: green;
          color: #fff ;
          padding: 5px 10px;
          font-size: .875rem;
          line-height: 1.5;
          border-radius:.2rem;
          vertical-align: middle;
          /* display: none!important; */
      }
      .email-show{
          display: none!important;
      }
  </style>
  

</head>

<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">

    <!-- Sidebar -->
    <?php require_once __DIR__."/../includes/sidebar.php"; ?>
    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">


      <!-- Topbar Navbar -->
        <?php require_once __DIR__."/../includes/navbar.php"; ?>

        </nav>
        <!-- End of Topbar -->

        <!-- Begin Page Content-->
        
        <!-- Page Heading -->
        <div class="container-fluid">
            <div class="d-sm-flex align-items-center justify-content-between ">
                <h1 class="h3 mb-4 text-gray-800">Sales</h1>
            </div>

        </div>
        <!-- /.container-fluid -->
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card show mb-4">
                        <!-- Card Header -->
                        <div class="card-header py-3 d-flex flex-row align-items-center justify-content-end">
                            <div>
                                <input type="email" class= "form-control"
                                name="email" id="customer_email"
                                placeholder="Enter Email Of Customer">
                            </div>
                            <div>
                                <p class="email-verify email-show" id="email_verify_success">
                                    <i class="fas fa-check fa-sm text-white nr-1"></i>Email Verified
                                </p>
                                <p class="email-verify bg-danger d-inline-block mb-0 email-show" id="email_verify_fail">
                                    <i class="fas fa-times fa-sm text-white nr-1"></i>Email Not Verified
                                </p>
                                <a href="<?=BASEPAGES;?>add-customer.php" class="btn btn-sm btn-warning shadow-sm d-inline-block email-show" id="add_customer_btn">
                            <i class="fas fa-envelope fa-sm text-white"></i>Add customer</a>
                            <button type="button" class="d-sm-inline-block btn btn-primary shadow-sm" name="check_email" id="check_email" onclick="getCustomerWithEmail()">
                                <i class="fas fa-envelope fa-sm text-white"></i>Check Email
                            </button>
                            </div>
                        </div>
                        <div class= "card-header py-3 d-flex flex-row align-items-center justify-content-between">
                            <h6 class="m-0 font-weight-bold text-primary">
                                <i class="fas fa-plus"></i>Sales
                            </h6>
                            <button type="button"
                            class="d-sm-inline-block btn btn-sm btn-secondary shadow-sm"
                            onclick="addProduct()">
                        <i class="fas fa-plus fa-sm text-white"></i>Add One More Product</button>
                        </div>
                        <!--END OF CARD HEADER-->

                       
                        <form action="<?= BASEURL?>helper/routing.php" method="POST">
                        <input type="hidden"
                              name="csrf_token"
                              value="<?= Session::getSession('csrf_token');?>">
                        <input type="text" name="customer_id" id ="customer_id">
                         <!--CARD BODY-->
                        <div class="card-body">
                          <!-- Start of product container -->
                            <div id="products_container">
                                <div class="row product_row" id="element_1">
                                    <!--BEGIN: CATEGORY SELECT-->
                                    <div class="col-md-2">
                                            <div class="form-group">
                                                <label for="">Category</label>
                                                <select id="category_1" class="form-control category_select">
                                                    <option disabled selected>Select Category</option>
                                                    <?php
                                                    $categories = $di->get('database')->readData("category", ['id','name'],"deleted=0");
                                                    foreach($categories as $category){
                                                        echo "<option value='{$category->id}'>{$category->name}</option>";
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                        <!--END: CATEGORY SELECT-->
                                    <!-- Product select -->
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="">Products</label>
                                            <select name="products[]" id="product_1" class="form-control product_select">
                                            <option disabled selected>Select Product</option>
                                            </select>
                                        </div>
                                    </div>
                                    <!--/ Product select -->
                                    <!-- Selling Price-->
                                    <div class="col-md-2">
                                        <div class="form-group ">
                                            <label for="">Selling Price</label>
                                            <input type="text"  id="selling_price_1"
                                            class="form-control selling_select"
                                            disabled>
                                        </div>
                                    </div>
                                    <!--/ Selling Price-->
                                    <!-- Quantity-->
                                    <div class="col-md-1">
                                        <div class="form-group ">
                                            <label for="">Quantity</label>
                                            <input type="number"  min="1" name="quantity[] " id="quantity_1"
                                            class="form-control calculations"
                                            value="0">
                                        </div>
                                    </div>
                                    <!--/ Quantity-->
                                    <!-- discount-->
                                    <div class="col-md-1">
                                        <div class="form-group">
                                            <label for="">Discount</label>
                                            <input type="number" name="discount[] " id="discount_1"
                                            class="form-control calculations"
                                            value="0">
                                        </div>
                                    </div>
                                    <!--/ discount-->
                                    <!-- Final Rate-->
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label for="">Final Price</label>
                                            <input type="text"  id="final_price_1"
                                            class="form-control final_price"
                                            disabled>
                                        </div>
                                    </div>
                                    <!--/ Final Rate-->
                                    <!-- Delete Button -->
                                                <div class="col-md-1">
                                                    <button onclick="deleteProduct(1)" type="button" class="btn btn-danger"
                                                    style="margin-top:45%"><i class ="far fa-trash-alt"></i></button>
                                                </div>
                                    <!-- /delete buttom -->
                                </div>
                            </div>
                            
                          
                        </div>
                        <!-- END OF CARD BODY -->

                        <!--BEGIN: CARD FOOTER-->
                              <div class="card-footer d-flex justify-content-between">
                                  <div>
                                      <input type="submit" class="btn btn-primary" name="add_sales" value="Submit">
                                              <!-- </form> -->
                                  </div>
                                  <div class="form-group final_select">
                                      <label for="final_total" class="col-sm-4 col-form-label">Total</label>
                                      <div class="col-sm-8">
                                          <input type="text" readonly class="form-control final_select" id="final_total" value="0">
                                      </div>
                                  </div>
                              </div>
                              <!--END: CARD FOOTER-->


                    </div>
                </div>
            </div>
        </div>
      <!-- End of Main Content -->
</div>
      <!-- Footer -->
      <?php require_once __DIR__."/../includes/footer.php"; ?>
      <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  
  <!-- <?php require_once __DIR__."/../includes/scroll-to-top.php"; ?> -->
  <?php require_once __DIR__."/../includes/core-scripts.php"; ?>

  <?php require_once __DIR__."/../includes/page-level/index-scripts.php"; ?>
  <!-- <script src="<?=BASEASSETS?>js/plugins/jquery-validation/jquery.validate.min.js"></script> -->
  <script src="<?=BASEASSETS?>js/pages/transactions/add-sales.js"></script>


</body>

</html>
