<?php
    require_once __DIR__."/../../helper/init.php";
    $page_title ="Quick ERP | Invoice";
    $sidebarSection = "Transaction";
    Util::createCSRFToken();
    $errors="";
    $old="";
    if(Session::hasSession('old'))
    {
      $old = Session::getSession('old');
      Session::unsetSession('old');
    }
    if(Session::hasSession('errors'))
    {
      $errors = unserialize(Session::getSession('errors'));
      Session::unsetSession('errors');
    }
    $inv_id = $_GET['id'];
    $customer = $di->get('sales')->getCustomerWithInvoiceID($inv_id);
    Util::dd($customer);
    $products = $di->get('sales')->getProductsWithInvoiceID($inv_id);
     Util::dd($products);
?>
<!DOCTYPE html>
<html lang="en">

<head>

  <?php
    require_once __DIR__."/../includes/head-section.php";
  ?>
</head>

<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">

    <!-- Sidebar -->
    <?php require_once __DIR__."/../includes/sidebar.php"; ?>
    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

        <!-- Topbar -->
          <?php require_once __DIR__."/../includes/navbar.php"; ?>
        <!-- End of Topbar -->

        <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          <h1 class="h3 mb-4 text-gray-800"></h1>

        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

      <!-- Footer -->
      <?php require_once __DIR__."/../includes/footer.php"; ?>
      <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  <?php require_once __DIR__."/../includes/scroll-to-top.php"; 
  ?>

 
  <?php require_once __DIR__."/../includes/core-scripts.php"; ?>

  <?php require_once __DIR__."/../includes/page-level/index-scripts.php"; ?>

</body>

</html>
