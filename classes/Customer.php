<?php
class Customer{
    private $table = "customer";
    private $addressTable = "address";
    private $address_customer ="address_customer";
    private $columns = ['id', 'first_name','last_name','gst_no','phone_no','email_id','gender'];
    protected $di;
    private $database;
    private $validator;
    protected $customer_email;
    public function __construct(DependencyInjector $di)
    {
        $this->di = $di;
        $this->database = $this->di->get('database');
    }
    public function getValidator(){
        return $this->validator;
    }
    public function ValidateData($data)
    {   
        $this->validator = $this->di->get('validator');
        $this->validator = $this->validator->check($data,[
            'first_name'=>[
                'required'=>true,
                'minlength'=>3,
                'maxlength'=>20,
                // 'unique'=>$this->table
            ],
            'last_name'=>[
                'required'=>true,
                'minlength'=>3,
                'maxlength'=>20,
                // 'unique'=>$this->table
            ],
            'gst_no'=>[
                'required'=>true,
                'minlength'=>10,
                'maxlength'=>10,
                // 'unique'=>$this->table

            ],
            'phone_no'=>[
                'required'=>true,
                'minlength'=>10,
                'maxlength'=>10,
                // 'unique'=>$this->table

            ],
            'email_id'=>[
                'required'=>true,
                'minlength'=>5,
                'unique'=>$this->table
            ],
            'gender'=>[
                'required'=>true
            ],
            'block_no'=>[
                'required'=>true,
                'minlength'=>3,
            ],
            'street'=>[
                'required'=>true,
                'minlength'=>3,
            ],
            'city'=>[
                'required'=>true,
                'minlength'=>3,
            ],
            'pincode'=>[
                'required'=>true,
                'maxlength'=>6,
            ],
            'state'=>[
                'required'=>true,
                'minlength'=>3,
            ],
            'country'=>[
                'required'=>true,
                'minlength'=>3,
            ],

        ]);
    }
    public function addCustomer($data){
        // die($data);
        //VALIDATE DATA
        $this->ValidateData($data);

        //Insert data in db
        if(!$this->validator->fails())
        {
            try{
                $this->database->beginTransaction();
                $data_to_be_inserted = [
                    'first_name'=>$data['first_name'],
                    'last_name'=>$data['last_name'],
                    'gst_no'=>$data['gst_no'],
                    'phone_no'=>$data['phone_no'],
                    'email_id'=>$data['email_id'],
                    'gender'=>$data['gender']
                ];
                $address_to_be_inserted=[
                    'block_no'=>$data['block_no'],
                    'street'=>$data['street'],
                    'city'=>$data['city'],
                    'pincode'=>$data['pincode'],
                    'state'=>$data['state'],
                    'country'=>$data['country']
                 
                ];
                

                $customer_id = $this->database->insert($this->table,$data_to_be_inserted);
                $customer_addr = $this->database->insert($this->addressTable,$address_to_be_inserted);
                $address_data_to_be_inserted =[
                    'address_id'=> $customer_addr, 
                    'customer_id'=>$customer_id
                ];
                $address_customer_id = $this->database->insert($this->address_customer,$address_data_to_be_inserted);
                $this->database->commit();
                return ADD_SUCCESS;
            }
            catch(Exception $e)
            {
                $this->database->rollBack();
                return ADD_ERROR;
            }
        }
        return VALIDATION_ERROR;

    }

    public function getJSONDataForDataTable($draw, $search_parameter, $order_by, $start,$length)
    {
    $query = "SELECT * FROM {$this->table} WHERE deleted = 0";
    // $addrCustTable = "SELECT * FROM {$this->table} WHERE customer_id";
    $addrquery = "SELECT * FROM {$this->addressTable} WHERE deleted = 0";
    $totalRowCountQuery = "SELECT COUNT(*) as total_count FROM {$this->table} WHERE deleted = 0";
    $filteredRowCountQuery = "SELECT COUNT(*) as total_count FROM {$this->table} WHERE deleted = 0";

    if($search_parameter != null)
    {
        $query .=" AND name LIKE '%{$search_parameter}%'";
        $filteredRowCountQuery .=" AND name LIKE '%{$search_parameter}%'";
    }
    if($order_by != null)
    {
        $query .= " ORDER BY {$this->columns[$order_by[0]['column']]} {$order_by[0]['dir']}";

        $filteredRowCountQuery .= " ORDER BY {$this->columns[$order_by[0]['column']]} {$order_by[0]['dir']}";

    }

    else{
        $query .= " ORDER BY {$this->columns[0]} ASC";
        $filteredRowCountQuery .=" ORDER BY {$this->columns[0]} ASC";
    }
    if($length !=-1)
    {
        $query .=" LIMIT {$start},{$length}";
    }
    $totalRowCountResult = $this->database->raw($totalRowCountQuery);
    $numberOfTotalRows= is_array($totalRowCountResult) ? $totalRowCountResult[0]->total_count :0;

    $filteredRowCountResult= $this->database->raw($filteredRowCountQuery);
    $numberOfFilteredRows = is_array($filteredRowCountResult) ? $filteredRowCountResult[0]->total_count :0;

    $fetchedData = $this->database->raw($query);
    $addrfetchedData = $this->database->raw($addrquery);
    // Util::dd($fetchedData);
    $data = [];
    $numRows = is_array($fetchedData) ? count($fetchedData) : 0;
    $basePages = BASEPAGES;
    for($i=0;$i<$numRows;$i++){
        $subArray = [];
        $subArray[] = $start+$i+1;
        $subArray[] = $fetchedData[$i]->first_name;
        $subArray[] = $fetchedData[$i]->last_name;
        $subArray[] = $fetchedData[$i]->gst_no;
        $subArray[] = $fetchedData[$i]->phone_no;
        $subArray[] = $fetchedData[$i]->email_id;
        $subArray[] = $fetchedData[$i]->gender;
        $subArray[] = $addrfetchedData[$i]->block_no . $addrfetchedData[$i]->street ."," .$addrfetchedData[$i]->city .",".$addrfetchedData[$i]->pincode .",".$addrfetchedData[$i]->state .",".$addrfetchedData[$i]->country;

        // Util::dd($addrfetchedData);
        $subArray[] = <<<BUTTONS
        <a href="{$basePages}edit-customer.php?id={$fetchedData[$i]->id}" class='btn btn-outline-primary btn-sm edit'><i class="fas fa-pencil-alt"></i></a>
        <button class='btn btn-outline-danger btn-sm delete' data-id='{$fetchedData[$i]->id}' data-toggle='modal' data-target='#deleteModal'><i class="fas fa-trash-alt"></i></button> 
BUTTONS;

        $data[] = $subArray;

    }

    $output = array(
        'draw'=>$draw,
        'recordsTotal'=>$numberOfTotalRows,
        'recordsFiltered'=>$numberOfFilteredRows,
        'data'=>$data
    );
    echo json_encode($output);
    }

    public function getCustomerByID($id, $fetchMode= PDO::FETCH_OBJ)
    {   
        $query = "SELECT * from {$this->table} WHERE id={$id} AND deleted = 0";
        $result = $this->database->raw($query,$fetchMode);
        return $result;
    }
    public function getCustomerWithEmail($email_id, $fetchMode= PDO::FETCH_OBJ,$id=5)
    { 
        $query = "SELECT * FROM {$this->table} WHERE email_id ='{$email_id}'";
        $result = $this->database->raw($query,$fetchMode);
        return $result;
    }
    public function getAddressCustomerByID($id, $fetchMode = PDO::FETCH_OBJ)
    {
    $query = "SELECT * FROM {$this->address_customer} WHERE id = {$id} ";
    $result = $this->database->raw($query,$fetchMode);
    return $result;
    // Util::dd($result);
    }
    public function getAddressById($id, $fetchMode = PDO::FETCH_OBJ){
    $query = "SELECT * FROM {$this->addressTable} WHERE id ={$id}";
    $result = $this->database->raw($query,$fetchMode);
    return $result;
    }


    public function update($data,$id)
    {
        // Util::dd($data);
        $customer_email = $data['email_id'];
        $customer_id = $data['id'];
        $customer_gst_no = $data['gst_no'];
        $address_id = $data['address_id'];
   
    $customer_email_query ="SELECT COUNT(*) as total_count from {$this->table} WHERE email_id = '$customer_email' and id != $customer_id ";

    // $customer_gst_query ="SELECT COUNT(*) as total_count from {$this->table} WHERE gst_no = '$customer_gst_no' and id != $customer_id ";
    $emailresult = $this->database->raw($customer_email_query);
    // Util::dd($customer_email);
    // $gstresult = $this->database->raw($customer_gst_query);
    // Util::dd($emailresult[0]->total_count);

        $this->ValidateData($data);

                
    // if(!$this->validator->fails() || ($emailresult[0]->total_count ==0 && $gstresult[0]->total_count == 0))
    if(!$this->validator->fails() || $emailresult[0]->total_count == 0 )
        {
            try{
                $this->database->beginTransaction();
                $data_to_be_updated['email_id'] =  $data['email_id'];
                $data_to_be_updated['first_name'] =  $data['first_name'];
                $data_to_be_updated['last_name'] =  $data['last_name'];
                $data_to_be_updated['gst_no'] =  $data['gst_no'];
                $data_to_be_updated['phone_no'] =  $data['phone_no'];
                $data_to_be_updated['email_id'] =  $data['email_id'];
                $data_to_be_updated['gender'] =  $data['gender'];
                $address_to_be_updated['block_no'] = $data['block_no'];
                $address_to_be_updated['street'] = $data['street'];
                $address_to_be_updated['city'] = $data['city'];
                $address_to_be_updated['pincode'] = $data['pincode'];
                $address_to_be_updated['state'] = $data['state'];
                $address_to_be_updated['country'] = $data['country'];
                $this->database->update($this->table,$data_to_be_updated,"id = {$id}");
                $this->database->update($this->addressTable,$address_to_be_updated,"id = {$address_id}");
                
                // Util::dd($data_to_be_updated);
                $this->database->commit();
                // Util::dd($data_to_be_updated);

                return UPDATE_SUCCESS;
            }catch(Exception $e){
                $this->database->rollBack();
                return UPDATE_ERROR;
            }
        }
        else{
            // Util::dd("hello");
            return VALIDATION_ERROR;
        }
        
    }


    public function delete($id)
    {
        try{
            $this->database->beginTransaction();
            $this->database->delete($this->table,"id={$id}");
            $this->database->commit();
            return DELETE_SUCCESS;
        }catch(Exception $e){
            $this->database->rollBack();
            return DELETE_ERROR;
        }
    }
    
}
?>
