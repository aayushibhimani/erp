<?php
class Supplier{
    private $table = "suppliers";
    private $addressTable = "address";
    private $address_supplier ="address_supplier";
    private $columns = ['id', 'first_name','last_name','gst_no','phone_no','email_id','company_name','block_no','street','city','pincode','state','country'];
    protected $di;
    private $database;
    private $validator;
    public function __construct(DependencyInjector $di)
    {
        $this->di = $di;
        $this->database = $this->di->get('database');
    }
    public function getValidator(){
        return $this->validator;
    }
    public function ValidateData($data)
    {   
        $this->validator = $this->di->get('validator');
        $this->validator = $this->validator->check($data,[
            'first_name'=>[
                'required'=>true,
                'minlength'=>3,
                'maxlength'=>20,
                // 'unique'=>$this->table
            ],
            'last_name'=>[
                'required'=>true,
                'minlength'=>3,
                'maxlength'=>20,
                // 'unique'=>$this->table
            ],
            'gst_no'=>[
                'required'=>true,
                'minlength'=>10,
                'maxlength'=>10
            ],
            'phone_no'=>[
                'required'=>true,
                'minlength'=>10,
                'maxlength'=>10
            ],
            'email_id'=>[
                'required'=>true,
                'minlength'=>5,
                'unique'=>$this->table
            ],
            'company_name'=>[
                'required'=>true,
                'minlength'=>3,
                'maxlength'=>20,
                // 'unique'=>$this->table
            ],
            'block_no'=>[
                'required'=>true,
                'minlength'=>3,
            ],
            'street'=>[
                'required'=>true,
                'minlength'=>3,
            ],
            'city'=>[
                'required'=>true,
                'minlength'=>3,
            ],
            'pincode'=>[
                'required'=>true,
                'maxlength'=>6,
            ],
            'state'=>[
                'required'=>true,
                'minlength'=>3,
            ],
            'country'=>[
                'required'=>true,
                'minlength'=>3,
            ],

        ]);
    }
    public function addSupplier($data){
        // die($data);
        //VALIDATE DATA
        $this->ValidateData($data);

        //Insert data in db
        if(!$this->validator->fails())
        {
            try{
                $this->database->beginTransaction();
                $data_to_be_inserted = [
                    'first_name'=>$data['first_name'],
                    'last_name'=>$data['last_name'],
                    'gst_no'=>$data['gst_no'],
                    'phone_no'=>$data['phone_no'],
                    'email_id'=>$data['email_id'],
                    'company_name'=>$data['company_name']
                ];
                $address_to_be_inserted=[
                    'block_no'=>$data['block_no'],
                    'street'=>$data['street'],
                    'city'=>$data['city'],
                    'pincode'=>$data['pincode'],
                    'state'=>$data['state'],
                    'country'=>$data['country']
                 
                ];

                $supplier_id = $this->database->insert($this->table,$data_to_be_inserted);
                $supplier_addr = $this->database->insert($this->addressTable,$address_to_be_inserted);
                $address_data_to_be_inserted =[
                    'address_id'=> $supplier_addr, 
                    'supplier_id'=>$supplier_id
                ];
                $address_supplier_id = $this->database->insert($this->address_supplier,$address_data_to_be_inserted);
                $this->database->commit();
              
                return ADD_SUCCESS;
            }
            catch(Exception $e)
            {
                $this->database->rollBack();
                return ADD_ERROR;
            }
        }
        return VALIDATION_ERROR;

    }

    public function getJSONDataForDataTable($draw, $search_parameter, $order_by, $start,$length)
    {
    $query = "SELECT * FROM {$this->table} WHERE deleted = 0";
    $addrquery = "SELECT * FROM {$this->addressTable} WHERE deleted = 0";
    // $addrtotalRowCountQuery = "SELECT COUNT(*) as total_count FROM {$this->addressTable} WHERE deleted = 0";
    $totalRowCountQuery = "SELECT COUNT(*) as total_count FROM {$this->table} WHERE deleted = 0";
    $filteredRowCountQuery = "SELECT COUNT(*) as total_count FROM {$this->table} WHERE deleted = 0";
    // $addrfilteredRowCountQuery = "SELECT COUNT(*) as total_count FROM {$this->addressTable} WHERE deleted = 0";


    if($search_parameter != null)
    {
        $query .=" AND name LIKE '%{$search_parameter}%'";
        // $addrquery .=" AND name LIKE '%{$search_parameter}%'";
        $filteredRowCountQuery .=" AND name LIKE '%{$search_parameter}%'";
        // $addrfilteredRowCountQuery .=" AND name LIKE '%{$search_parameter}%'";

    }
    if($order_by != null)
    {
        $query .= " ORDER BY {$this->columns[$order_by[0]['column']]} {$order_by[0]['dir']}";
        // $addrquery .= " ORDER BY {$this->columns[$order_by[0]['column']]} {$order_by[0]['dir']}";
        $filteredRowCountQuery .= " ORDER BY {$this->columns[$order_by[0]['column']]} {$order_by[0]['dir']}";
        // $addrfilteredRowCountQuery .= " ORDER BY {$this->columns[$order_by[0]['column']]} {$order_by[0]['dir']}";

    }

    else{
        $query .= " ORDER BY {$this->columns[0]} ASC";
        // $addrquery .= " ORDER BY {$this->columns[0]} ASC";
        $filteredRowCountQuery .=" ORDER BY {$this->columns[0]} ASC";
        // $addrfilteredRowCountQuery .=" ORDER BY {$this->columns[0]} ASC";

    }
    if($length !=-1)
    {
        $query .=" LIMIT {$start},{$length}";
        // $addrquery .=" LIMIT {$start},{$length}";

    }
    $totalRowCountResult = $this->database->raw($totalRowCountQuery);
    // $addrtotalRowCountResult = $this->database->raw($addrtotalRowCountQuery);
    $numberOfTotalRows= is_array($totalRowCountResult) ? $totalRowCountResult[0]->total_count :0;
    // $addrnumberOfTotalRows= is_array($addrtotalRowCountResult) ? $addrtotalRowCountResult[0]->total_count :0;

    $filteredRowCountResult= $this->database->raw($filteredRowCountQuery);
    // $addrfilteredRowCountResult= $this->database->raw($addrfilteredRowCountQuery);

    $numberOfFilteredRows = is_array($filteredRowCountResult) ? $filteredRowCountResult[0]->total_count :0;
    // $addrnumberOfFilteredRows = is_array($addrfilteredRowCountResult) ? $addrfilteredRowCountResult[0]->total_count :0;


    $fetchedData = $this->database->raw($query);
    $addrfetchedData = $this->database->raw($addrquery);
    $data = [];
    $numRows = is_array($fetchedData) ? count($fetchedData) : 0;
    for($i=0;$i<$numRows;$i++){
        $subArray = [];
        $subArray[] = $start+$i+1;
        $subArray[] = $fetchedData[$i]->first_name;
        $subArray[] = $fetchedData[$i]->last_name;
        $subArray[] = $fetchedData[$i]->gst_no;
        $subArray[] = $fetchedData[$i]->phone_no;
        $subArray[] = $fetchedData[$i]->email_id;
        $subArray[] = $fetchedData[$i]->company_name;
        $subArray[] = $addrfetchedData[$i]->block_no;
        $subArray[] = $addrfetchedData[$i]->street;
        $subArray[] = $addrfetchedData[$i]->city;
        $subArray[] = $addrfetchedData[$i]->pincode;
        $subArray[] = $addrfetchedData[$i]->state;
        $subArray[] = $addrfetchedData[$i]->country;

        $subArray[] = <<<BUTTONS
        <button class = 'btn btn-outline-primary btn-sm' data-id='{$fetchedData[$i]->id}'><i class="fas fa-pencil-alt"></i></button>
        <button class = 'btn btn-outline-danger btn-sm' data-id='{$fetchedData[$i]->id}'><i class="fas fa-trash-alt"></i></button>
BUTTONS;

        $data[] = $subArray;

    }

    $output = array(
        'draw'=>$draw,
        'recordsTotal'=>$numberOfTotalRows,
        'recordsFiltered'=>$numberOfFilteredRows,
        'data'=>$data
    );
    echo json_encode($output);
    }
}
?>


