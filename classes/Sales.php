<?php
class Sales
{
    private $salesTable = "sales";
    private $invoiceTable = "invoice";
    private $customerTable = "customer";
    private $customerAddressTable = "address_customer";
    PRIVATE $addressTable = "address";
    private $productsTable = "products";
    private $priceTable = "products_selling_rate";

    protected $di;
    private $database;
    private $validator;
    public function __construct(DependencyInjector $di)
    {
        $this->di = $di;
        $this->database = $this->di->get('database');
    }
    public function getValidator()
    {
        return $this->validator;
    }
    public function create($data)
        {   
            $products = $data['products'];
            $length = sizeof($products);
            // Util::dd($length);
        try{
            $this->database->beginTransaction();
            $customer_id = ['customer_id' => $data['customer_id']];
            $invoice_id = $this->database->insert($this->invoiceTable,$customer_id);
            for($i=0; $i<$length; $i++)
            {
                // Util::dd($data['products'][$i]);
                $data_to_be_inserted = "";
                $data_to_be_inserted = [
                    'product_id'=>$data['products'][$i],
                    'quantity'=>$data['quantity'][$i],
                    'discount'=>$data['discount'][$i],
                    'invoice_id'=>$invoice_id,
                ];
                $sales_id = $this->database->insert($this->salesTable,$data_to_be_inserted);
                // $_POST[$invoice_id];
            }
            
            // Util::dd($invoice_id);
            $this->database->commit();
            return ADD_SUCCESS;
        }
        catch(Exception $e){
            $this->database->rollBack();
                return ADD_ERROR;
        }
    }
    public function getInvoiceID()
    {
        $invoice_id_query = "SELECT MAX(id) as id FROM {$this->invoiceTable} WHERE deleted =0";
        $invoice_id = $this->database->raw($invoice_id_query);
        // Util::dd($invoice_id[0]->id);
        return $invoice_id[0]->id;
    }
    public function getCustomerWithInvoiceID($id,$fetchMode= PDO::FETCH_OBJ)
    {
    $customer_id_query = "SELECT customer_id FROM {$this->invoiceTable} WHERE id = '$id'";
    $customer_id_array = $this->database->raw($customer_id_query);
    $customer_id =  $customer_id_array[0]->customer_id;
    // Util::dd($customer_id);
    $customer_query = "SELECT * FROM {$this->customerTable} WHERE id='$customer_id'";
    $customer_array = $this->database->raw($customer_query);
    $address_id_query = "SELECT address_id FROM {$this->customerAddressTable} where id = $customer_id";
    $address_id_array =  $this->database->raw($address_id_query);   
    $address_id = $address_id_array[0]->address_id;
    $address_query = "SELECT * FROM {$this->addressTable} where id = $address_id and deleted = 0";
    $address_array = $this->database->raw($address_query);
    $customer_details[] = [
        'name'=>$customer_array[0]->first_name ." ". $customer_array[0]->last_name,
        'gst_no'=>$customer_array[0]->gst_no,
        'email_id'=>$customer_array[0]->email_id,
        'phone_no'=>$customer_array[0]->phone_no,
        'address'=>$address_array[0]->block_no ." ".$address_array[0]->street. " ".$address_array[0]->city. " ".$address_array[0]->pincode. " ".$address_array[0]->state. " ".$address_array[0]->country
    ];
    // Util::dd($customer_details);
    return $customer_details;
    }

    public function getProductsWithInvoiceID($id)
    {
        $products_id_query = "SELECT product_id FROM {$this->salesTable} WHERE invoice_id = '$id'";
        $products_id_array = $this->database->raw($products_id_query);
        foreach($products_id_array as $product_id){
            $products_query = "SELECT * FROM {$this->productsTable} WHERE id = '$product_id->product_id'";
            $products_array= $this->database->raw($products_query);
            $price_query = "SELECT selling_rate FROM {$this->priceTable} WHERE product_id = '$product_id->product_id'";
            $price_array = $this->database->raw($price_query);
            $product[] = [
                'name'=>$products_array[0]->name,
                'specification'=>$products_array[0]->specification,
                'hsn_code'=>$products_array[0]->hsn_code,
                'price'=>$price_array[0]->selling_rate
            ];
          
        }
         return $product;
    }

}