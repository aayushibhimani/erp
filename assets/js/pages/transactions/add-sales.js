var id = 2;
var total_id =id;
var baseURL = window.location.origin;
 var filePath = "/helper/routing.php";
function deleteProduct(delete_id) {
    var elements = document.getElementsByClassName("product_row");
    if(elements.length != 1) {
        $("#element_" + delete_id).remove();
        calculateTotal();
    }
}
function addProduct() {
    // console.log(id);
    $("#products_container").append(
        `<!-- PRODUCT CUSTOM CONTROL -->
        <div class="row product_row" id="element_`+id+`">
            <!-- CATEGORY SELECT -->
            <div class="col-md-2">
                <div class="form-group">
                    <label for="">Category</label>
                    <select id="category_`+id+`" class="form-control category_select">
                        <option disabled selected>Select Category</option>
                        
                    </select>
                </div>
            </div>
            <!-- /CATEGORY SELECT -->
            <!-- PRODUCTS SELECT -->
            <div class="col-md-3">
                <div class="form-group product_select">
                        <label for="">Products</label>
                        <select name="products[]" id="product_`+id+`" class="form-control product_select">
                            <option disabled selected>Select Product</option>
                        </select>
                </div>
            </div>
            <!-- /PRODUCTS SELECT -->
            <!-- SELLING PRICE -->
            <div class="col-md-2">
                <div class="form-group ">
                    <label for="">Selling Price</label>
                    <input type="text" id="selling_price_`+id+`" class="form-control selling_select" value="" disabled>
                </div>
            </div>
            <!-- /SELLING PRICE -->
            <!-- QUANTITY -->
            <div class="col-md-1">
                <div class="form-group ">
                    <label for="">Quantity</label>
                    <input type="number" min="1"  name="quantity[]" id="quantity_`+id+`" value="0" class="form-control calculations">
                </div>
            </div>
            <!-- /QUANTITY -->
            <!-- DISCOUNT -->
            <div class="col-md-1">
                <div class="form-group">
                    <label for="">Discount</label>
                    <input type="number" name="discount[]" id="discount_`+id+`" class="form-control calculations" value="0">
                </div>
            </div>
            <!-- /DISCOUNT -->
            <!-- FINAL PRICE -->
            <div class="col-md-2">
                <div class="form-group">
                    <label for="">Final Price</label>
                    <input type="text" id="final_price_`+id+`" class="form-control final_price" disabled>
                </div>
            </div>
            <!-- /FINAL PRICE -->
            <!-- DELETE BUTTON -->
            <div class="col-md-1">
                <button onclick="deleteProduct(`+id+`)" type="button" class="btn btn-danger" style="margin-top: 43%;"> 
                    <i class="far fa-trash-alt"></i>
                </button>
            </div>
            <!-- /DELETE BUTTON -->
        </div>
        <!-- /PRODUCT CUSTOM CONTROL -->`
    );
    $.ajax({
        url: baseURL+filePath,
        method: 'POST',
        data:{
            getCategories: true
        },
        dataType: 'json',
        success: function(categories){
            categories.forEach(function (category){
                // console.log(category);
                $("#category_"+id).append(
                    `<option value='${category.id}'>${category.name}</option>`
                );
            });
            id++;
            total_id++;
        }
    });
    
}

// Event Delegation
$("#products_container").on('change', '.category_select', function(){
    // console.log(this);
    var element_id = $(this).attr('id').split("_")[1];
    var category_id = this.value;
    //  console.log(category_id);
    $.ajax({
        url: baseURL+filePath,
        method: 'POST',
        data:{
            getProductsByCategoryID: true,
            categoryID: category_id
        },
        dataType: 'json',
        success:function(products)
        {
            //Empty it first or else it will append the values to the previos values if category changes
                 console.log(category_id);
            $("#product_"+element_id).empty();
            $("#product_"+element_id).append("<option disabled selected>Select Product</option>");
            products.forEach(function (product){
                $("#product_"+element_id).append(
                    `<option value='${product.id}'>${product.name}</option>`
                );
            });
        },
        error:function(thrownError){
            console.log(thrownError);
        }
    });
});


//the above on is the second syntax of event handling which will work only if the target is present here 'category_select'
//If not AJAX one can use SESSIONs 
//Common Thing- id i.e product_id, category_id

$("#products_container").on('change', '.product_select',function(){
    // console.log(this);
    var element_id = $(this).attr('id').split("_")[1];
    var product_id = this.value;
    // console.log(element_id);
    $.ajax({
        url:baseURL+filePath,
        method:'POST',
        data:{
            getPriceByProductID: true,
            productID: product_id
        },
        dataType: 'json',
        success:function(product)
        {   
            // console.log('aayushi');
            $("#selling_price_"+element_id).empty();
            $("#selling_price_"+element_id).val(product[0].selling_rate);
           
        }
    });
    calculateTotal();
});


$("#products_container").on('change', '.calculations', function(){
    // console.log(this);
    var element_id = $(this).attr('id').split("_")[1];
    var final_price = setFinalPrice(element_id);
     //  console.log(final_price);
     $("#final_price_"+element_id).val(final_price);
     calculateTotal();
});

function setFinalPrice(element_id)
{
    var selling_price = $("#selling_price_"+element_id).val();
    var quantity = $("#quantity_"+element_id).val()
    var discount = $("#discount_"+element_id).val();
    if(quantity == 0)
    {
        quantity = 1;
    }
    return selling_price*quantity- ((discount/100)*selling_price*quantity);


}

function calculateTotal()
{
    var total_amount =0;
    for(i=1;i<=total_id;i++)
    {
        temp = document.getElementById("final_price_"+i);
        // console.log(temp)
        if(temp!=null)
        {
            final_price = document.getElementById("final_price_"+i).value;
            // console.log(final_price);
            total_amount += parseInt(final_price);

        }
    }
    $("#final_total").val(total_amount);

    
}


function getCustomerWithEmail()
{
    email_id= document.getElementById("customer_email").value;
    customer_id = document.getElementById('customer_id');
    // console.log(email_id)
    if(email_id =='')
    {
        alert('Enter a valid email address')
    }
    else{
        $.ajax({
            url:baseURL+filePath,
            method:'POST',
            data:{
                getCustomerWithEmail: true,
                emailID: email_id
            },
            dataType: 'json',
            success:function(customer)
            {   

                if(typeof customer !== 'undefined' && customer.length >0)
                {
                    // console.log(customer);
                $("#customer_id").val(customer[0].id);
                customer_id.setAttribute('type','hidden');
                $('#email_verify_success').removeClass('email-show');
                $('#email_verify_fail').addClass('email-show');
                    $('#add_customer_btn').addClass('email-show');
                }
                else{
                    $('#email_verify_success').addClass('email-show');
                    $('#email_verify_fail').removeClass('email-show');
                    $('#add_customer_btn').removeClass('email-show');
                }
            }
        });
    }
    
}   







